var diameter = 100;

var svg = d3.select("svg")
		.attr("width", '100%')
		.attr("height", '90vh')
		.attr("class", "bubble");

function draw(dataset){
	var color = d3.scaleOrdinal(d3.schemeCategory20);

	var bubble = d3.pack(dataset)
		  .size([window.innerWidth, window.innerHeight - window.innerWidth/15])
		.padding(1.5);

	var nodes = d3.hierarchy(dataset)
		  .sum(function(d) { return d.Count; });
     

	  var node = svg.selectAll(".node")
        .remove()
        .exit()
		.data(bubble(nodes).descendants())
		.enter()
		.filter(function(d){
			return  !d.children
		})
		.append("g")
		.attr("class", "node")
		.attr("transform", function(d) {
			return "translate(" + d.x + "," + d.y + ")";
		});

	node.append("title")
		.text(function(d) {
			return d.Name + ": " + d.Count;
		});

	node.append("circle")
		.attr("r", function(d) {
			return d.r;
		})
		.style("fill", function(d,i) {
			return color(i);
		});

	node.append("text")
		.attr("dy", ".2em")
		.style("text-anchor", "middle")
		.text(function(d) {
			return d.data.Name.substring(0, d.r / 3);
		})
		.attr("font-family", "sans-serif")
		.attr("font-size", function(d){
			return d.r/5;
		})
		.attr("fill", "white");

	node.append("text")
		.attr("dy", "1.3em")
		.style("text-anchor", "middle")
		.text(function(d) {
			return d.data.Count;
		})
		.attr("font-family",  "Gill Sans", "Gill Sans MT")
		.attr("font-size", function(d){
			return d.r/5;
		})
		.attr("fill", "white");

	d3.select(self.frameElement)
		    .style("height", "90vh");
}

// https://api.data.lectur.begel.fr/auteurs/author_nb_books/
// https://api.data.lectur.begel.fr/auteurs/author_nb_books/?page=2
//
// get n books per them
// https://api.data.lectur.begel.fr/livres/for_theme/?theme=12&limit=10

get_data("https://api.data.lectur.begel.fr/auteurs/author_nb_books/", []);
//draw(dataset);

function get_data(url, lst){
    d3.json(url, function(data){
		  console.log(data.results[0]);
      for (let i = 0; i < 10; i++) {
          let name = data.results[i].prenom + " " + data.results[i].nom; 
          let nb_books = data.results[i].nb_books;
          lst.push({"Name":name, "Count":nb_books});
      }

        draw({"children":lst});
        get_data(data.next, lst);
    });
}
