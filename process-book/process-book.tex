\documentclass[11pt, a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{url}
\usepackage{color}
\usepackage{ifthen}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\newcommand{\todo}[2][{Guillaume}]{
    \begingroup
        \let\SavedNewLine\\%
        \DeclareRobustCommand{\\}{\SavedNewLine}%
        {\LARGE \color{red}TODO @#1: #2}%
    \endgroup
}
\newcommand{\suggest}[2][{Guillaume}]{
    {\Large \color{green}$\xrightarrow{#1}$ #2}
}
\newcommand{\remark}[2][{Guillaume}]{
    {\large \color{blue}(Rq from #1: #2)}
}

\graphicspath{{img/}}


\title{A World of Books}
%\subtitle{Project for the course of Data Visualization}
\author{%
    Arnaud \textsc{Garin}
    Grégoire \textsc{Hirt}
    Guillaume \textsc{Vizier}
}

\begin{document}
\maketitle

\tableofcontents
\clearpage

\section*{Introduction}\label{sec:intro}
    \paragraph{Overview}Books have existed in various forms for more than three thousand years. %In order to find new books, there did not exist until recently any alternative to looking for new books in libraries, or asking people around us for their readings. With the advent of the new technologies, several websites were created to connect readers with one another. 
    There exist books about pretty much anything, but there is no way for a user to discover new books other than exchanging with other readers or going to a library or bookstore.

    \paragraph{Motivation}
    Having retrieved books from websites presenting books, we aimed at allowing readers to explore a dataset of books based on their subject, in order to either find books they might like, or to try books about slightly different subjects.

    \paragraph{Target audience} Our data visualization is aimed at any person wishing to find new books, discover new themes, or gain insight on the evolution of a theme through time.

\section{First steps}\label{sec:first-steps}
    \subsection{Wishes}\label{subsec:what}
    We got started on this project with the desire to give life to a 
    dataset that one of the team members had already constituted. Being all
    interested in reading literature, we hoped to convey most of this
    passion through a visualization of the relations between books.


    \subsection{Data}\label{subsec:data}
    Our data consists of books: several different websites help users find
    new books, and exchange with their peers about the books they read,
    liked, etc, like Goodreads~\cite{GR}, Booknode~\cite{BK},
    Babelio~\cite{B}, ... Other websites sell books, like Amazon~\cite{Am},
    Fnac~\cite{F}, ...
    The aforedescribed types of website both present books, but in totally
    different fashion: while the first ones usually consider an item based on
    its content (different editions or translations of the same book are
    usually linked together), the second ones consider them as articles.
    A drawback of most content-based website is the flaws in their data
    structure, and the lack of support for multiple languages.

    Thus, a database collecting the items from 3 different websites has been
    constituted (and is still being constituted) through scraping and
    automated cleaning.
    It appears that the cleaning process is not currently powerful enough to
    obtain completely clean data.
    Also, some situations require a human intervention.

    We used this partly cleaned, evolving dataset for our visualization.
    We focused mainly on the themes attributed to books by users, the author(s)
    of a book, and their grade (aggregated per website).

    We limited our visualization to the use of content in French and English,
    as detected by the automatic cleaning process.

    \subsection{First steps}\label{subsec:first-vizs}
        We initially created three different visualizations, as proofs
        of concept, and in order to get more insights on which part of the
        data is the most interesting one.
        \subsubsection{Graph of themes}\label{subsubsec:theme-graph-initial}
        \begin{figure}
          \centering
          \includegraphics[width=.9\textwidth]{img/early_theme_graph.png}
          \caption{Part of the early graph of the books themes connectivity}
          \label{fig:early-theme-graph}
        \end{figure}

        We were interested in the overall connectivity of the books themes. To
        get an idea, we quickly deployed a visualization of the themes.
        This visualization was a force directed graph where each node is a theme
        and edges are added if the themes have been attributed to the same book.
        The primitive criterion was a minimum number of shared books. As we
        can see in Figure \ref{fig:early-theme-graph}, where the names are
        omitted, the graph is a bit overconnected. But this did not discouraged
        us to make more experiments with the idea of theme graphs.

        \subsubsection{Bubbling of authors}\label{subsubsec:author-bubble}
        In an attempt to find a meaningful representation of the data, one idea
        was to create a bubble graph to represent each author relative to the
        amount of books they published: the larger the bubble, the bigger the
        number of publications.

        \begin{figure}
          \centering
          \includegraphics[width=.9\textwidth]{img/author_bubble_graph.png}
          \caption{Early representation of authors relative to their
            publications}
          \label{fig:author-bubble-graph}
          \end{figure}

        This visualization is visually appealing. However it
        has some downsides. Many of the less productive authors are shadowed
        by the most prolific ones. Also, since quantity does not equal quality, the
        metrics used for the bubble size might leave great writers with few books unnoticed.

        In the end, to still exploit the idea, we found that filtering the
        number of authors via themes and size the bubbles via the quality of
        their work would be the way to go.

        \subsubsection{Stacked chart for the themes}\label{subsubsec:stacked-chart}
            We wanted to explore the feasibility of a representation of the
            evolution in the number of existing books associated with a given
            theme. The main idea was to be able to discern trends in the
            themes of the published books.
            
            We wanted to be able to see if, for example, the vampire-mania was
            detectable, and if other such evolution could be spotted through
            our visualization.

            We thus plotted in a stacked chart the evolution of the number of
            books published before a date, for each theme, as presented in
            Figure~\ref{fig:stacked-chart}.
            We rapidly realized that we would need to filter a lot on the
            displayed themes: back then, there were around two thousand
            distinct themes in the dataset we use, and thus plotting a chart
            with all themes would have been worthless.
            Thus we had to figure out a way of keeping only the most
            interesting themes.

            \begin{figure}
                \centering
                \includegraphics[width=.9\textwidth]{stacked-chart.png}
                \caption{First draft for a themes timeline}\label{fig:stacked-chart}
            \end{figure}

            Another issue was performance: the server hosting the dataset is
            not powerful enough for us to retrieve all published books before
            a certain date for a theme, if it is for each theme and for a large
            range of dates.
            Thus we first decided to limit the number of themes, and the range
            of dates.
            This proved to still be long (around 10 seconds to get data for one
            hundred dates and for only one theme), thus we chose to focus on a
            different aspect of the books.

        \subsubsection{Word-cloud}\label{subsubsec:word-cloud}
            Having access to summaries for each book, we thought about displaying
            the most relevant words extracted from the summaries.
            For this purpose, we gathered for a theme the summaries of all books
            in French or English, extracted the words with the highest frequency
            after having removed the stopwords, and used the D3 library for word
            clouds~\cite{WC}.

            Having usually several summaries per book (311,461 summaries for 158,448 books,
            which represents two summaries for each book in average), we faced
            similar issues as with the stacked chart for the themes, described
            in Section~\ref{subsubsec:stacked-chart}, mainly performance.
            In addition to this problem, we realized that the existing lists of stopwords are not
            sufficient to filter out all meaningless words from the aggregated
            summaries: as precised on the Figure~\ref{fig:word-cloud}, we have
            a lot of words that are not usually considered as stopwords (like
            ``included'' and ``year''), but that do not have any meaning in
            our case.
            \begin{figure}
                \includegraphics[width=\textwidth]{word-cloud.png}
                \caption{Word-cloud for the theme Fantasy}\label{fig:word-cloud}
            \end{figure}

            In addition, we have seen during the lectures that word clouds are
            not very insightful, and can be tricky to analyze. Thus we decided
            to not go though the word-cloud approach.

\section{Core}\label{sec:core}

    In this section, we described the thought process that led from our first
    visualizations to our final result.

    \subsection{Design decisions}\label{subsec:design-decisions}
        After our initial steps, described in Section~\ref{sec:first-steps},
        we decided to focus on the themes: the links between themes, the most
        representative books for a theme, and an evolution themes through time.

        Due to the rapid cluttering of the graph of themes, we had to
        artificially reduce the connectivity among the themes. We thus
        decided to consider for each theme a reduced number of neighbors. We
        also reduced the number of themes displayed: while writing this paper,
        there are 3,436 themes in the dataset, which is too much to be
        reasonably displayed and readable by a user.

        We also wanted our users to have an idea of which books can be
        classified as belonging to a given theme. We thus decided to allow
        users to access the books related to a theme, and had to limit once
        again the number of books displayed. We initially retrieved only the
        most popular, the popularity being estimated by the following formula:
        $\mathtt{grade} * \mathtt{log}\left( \mathtt{number~of~votes} \right)$.
        We were later able to add other metrics, namely $\mathtt{grade}$ and
        $\mathtt{number~of~votes}$.

        We also wanted to allow users to display additional information on a book, and decided to present this information in a side panel.

    \subsection{Implementation}\label{subsec:implementation}
        In this section, we describe the different parts of our
        implementation, and the major difficulties encountered.
        \subsubsection{Graph of themes}\label{subsubsec:theme-graph}

            We kept our graph of themes idea as a final feature but prevented the
            unreadability through several adjustments :

            \begin{itemize}
              \item The number of themes displayed is restricted. The metric is the
                distance from a user-chosen central theme that acts as an entry
                point into the database. In the end, only themes with a distance of maximum 4 hops from the central theme are displayed
            \item Each theme is connected only to its $N$ most-related neighbors (after several trials, we kept $N = 5$, as it seemed the best value to still see clusters, but was low enough to prevent an explosion of the total number of retrieved themes).
              \item Force directed layout parameters are finely tuned.
            \end{itemize}

            \paragraph{Queries} To retrieve the theme graph from the distant
            database we do recursive queries. The server gives themes one at a time
            along with its $N$ nearest neighbors. We query the central theme, which is chosen by the
            user and then query all the neighbors for which our criterion
            holds.

            \paragraph{Display} The graph is displayed as a SVG element and animated
            using the D3 force directed graph system. Each node is a group element
            with a circle and a text. The edges are displayed using simple lines. While
            the graph is loaded incrementally and asynchronously, we take advantage of
            the D3 easy data management to remove and add the nodes as the graph evolves.

            \paragraph{Interactions} Interactions with the graph are pretty
            straightforward. They are implemented using \emph{onClick} events on the
            different elements. Clicking on a node produces two different behaviors,
            whether the node is the main central theme or a related theme. On one
            hand, if the node is the main theme, clicking on it opens a second
            visualization (described in Section~\ref{subsubsec:books}) of the books tagged with this particular theme, on the
            other hand, if the node is a related theme, it makes the new
            central theme from it and the interaction loop starts again. This allows the user to
            explore themes by iteratively move through related themes.

            Additionaly, the user can zoom in, zoom out and drag the graph with his mouse as long
            as it does not hover a node.

            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{theme_graph_final.png}
                \caption{Final look of the themes graph}
                \label{fig:final-theme-graph}
            \end{figure}

        \subsubsection{Overview of the books}\label{subsubsec:books}

        \paragraph{Initial layout} We saw great potential in the representation of a bubble graph.
        Nevertheless, as stated before, even if the provided interesting insights, a lot of information was lost due to the sheer size of the data and
        metrics of questionable quality. However, once we started filtering authors
        and their books with regard to their respective themes,  the problem of
        having points lost into the immensity of the graph was solved. Now the bubble graph
        represent individual books related to a theme. The radius given to a
        particular bubble depends now on a metric chosen by the user from some that offer very few variance to others that display differences in a more visual way. The available metrics are the number of grades, and the difference of the mean grade of a book to the mean of all books for this theme. These two metrics were chosen for the insight they provide depending on the order in which the books are retrieved (see Section~\ref{subsec:design-decisions}).

        \begin{figure}[ht]
          \includegraphics[width=\textwidth]{img/bubble_books}
          \caption{Bubble graph of books for theme Fiction}
          \label{fig:books-bubble-graph}
          \end{figure}

        \paragraph{Covers visualization} Displaying books in bubbles is good, but
        showing their cover instead of plain color background is better! Behind
        a relatively trivial statement, putting a cover as the background of a
        bubble was way more difficult than expected. We needed several specific
        attributes and links before finally coming to a result.

        \paragraph{Individual book look up}
        From the circular form of the nodes and the partial view of the covers we
        figured out that the natural way to interact with the graph would be
        clicking on a bubble to access more detailed information about the book. Thus, on click, a panel located on the verge of the visualization
        displays the full cover, title, author, serie and
        summary of the book. These informations are meant for readers who want to either
        confirm what they already know the book or discover new books related to their
        favourite theme.

        \subsubsection{Streamgraph}\label{subsubsec:stream-graph}
            As we were able to have the two aforementioned visualizations
            working faster than expected, we added a third visualization: we
            used our initial idea of displaying a timeline for the themes,
            but had to rethink how the data was queried.

            For the visualization in itself, we mainly reused the code from
            the initial stacked chart: changing the parameter
            \texttt{stackOffset} of the \texttt{stack} structure from
            \texttt{stackOffsetNone} to \texttt{stackOffsetWiggle} was enough.

            However,on the contrary of the initial chart, for which the labels
            were positioned outside of the graph, we used the
            \texttt{d3-area-label}~\cite{DAL} library to place our labels
            inside the areas, at the location offering the most space, as
            shown on Figure~\ref{fig:label-stream-graph}.

            \begin{figure}[ht]
                \includegraphics[width=\textwidth]{label-stream-graph.png}
                \caption{The labels of the areas of our stream graph are placed
                inside the corresponding area.}\label{fig:label-stream-graph}
            \end{figure}

            \paragraph{Changing the time axis}We added the possibility for a
            user to resize the time axis to focus one this or that period of
            time. We allow a user to change both the first and last displayed
            date, by dragging the axis to the left or the right. More precisely,
            to change the first display date, users have to grab anywhere on
            the left part of the axis, and can them move the axis around.
            Grabbing the axis on the right allows to change the last
            displayed date.
            The additional data that we might need to display the streamgraph
            is dynamically loaded from the server: we keep track of the timed
            data we already have retrieved from the server, and load only the
            time range that we do not already possess.

            If a user wants to keep the same time range (let us say 2 years),
            but move the displayed time window, eg from 2014-2016 to 2012-2014,
            this is also possible: he just need to grab any area of the stream graph,
            and move the chart to the right.

            \paragraph{Navigation}To improve the navigation inside the stream
            graph, we highlighted the areas on hover, by reducing the opacity
            of the non-hovered areas, as shown in Figure~\ref{fig:hovered-stream-graph}.

            \begin{figure}[ht]
                \includegraphics[width=\textwidth]{hovered-stream-graph.png}
                \caption{The hovered area of our stream graph is highlighted.}\label{fig:hovered-stream-graph}
            \end{figure}

            You can also see on Figure~\ref{fig:hovered-stream-graph} that a
            tooltip is present when an area is hovered over: as the
            implementation of this tooltip is common to all three visualizations,
            we will discuss in Section~\ref{subsubsec:additional-features}.

            \paragraph{Changing the themes} To avoid cluttering, we limited
            to 10 the number of themes initially displayed on the graph. Users
            can then change the displayed themes by selecting from the
            multi-selection button below the stream-graph the theme(s) they
            want to add or remove from the chart. Note that a user is allowed
            to add more than 10 themes to the stream graph: the cluttering is
            then only the user fault. The selection of themes for the stream
            graph has a search functionality, which allow looking for theme
            names containing a certain pattern, as for the visualization
            described in Section~\ref{subsubsec:theme-graph}.

            \paragraph{Changing the insight given by the displayed data}
            Initially, we displayed in the stream graph the number of books
            labeled after a given theme, for each month or year. We added later a second option. Thus we can display in the stream graph:
            \begin{itemize}
                \item either the number of books per theme
                \item or the number of books of the selected theme that are also labeled with the theme displayed in the stream graph.
            \end{itemize}
            This allows users to either compare the global evolution of the number of books per theme, or this evolution within the selected theme.

        \subsubsection{Additional features}\label{subsubsec:additional-features}
            \paragraph{Zoom and dragging}
                Zoom and drag implementation was straightforward. This solely consists in
                a transparent background that intercepts the needed mouse events. These
                events are captured by a D3 \emph{zoomBehaviour} and processed
                accordingly, generating a transformation. This transformation is then
                applied to the SVG node containing the visualization element that should
                be transformed (all the visualization in our case).

            \paragraph{Search}
                Given the huge number of themes we have, we had to find a way
                to allow users to find easily and quickly the theme they are
                looking for, as presented in Figure~\ref{fig:search}.

                \begin{figure}
                    \centering
                    \begin{subfigure}{.49\textwidth}
                        \centering
                        \includegraphics[width=\textwidth, keepaspectratio]{search-troll.png}
                    \end{subfigure}%
                    \hfill
                    \begin{subfigure}{.49\textwidth}
                        \centering
                        \includegraphics[width=\textwidth, keepaspectratio]{search-comique.png}
                    \end{subfigure}%
                    \caption{Search examples}\label{fig:search}
                \end{figure}

                The solution we found was an autocompletion input, or
                typeahead. This item is used in two places: on the theme
                graph screen, as a single selection tool to choose the
                central theme for the graph ; and on the stream graph
                screen, as a multiple selection tool to choose the themes
                to display on the stream graph.

                We used the JavaScript libraries
                \texttt{bootstrap-select}~\cite{BS}
                for the multi-select input, and
                \texttt{Bootstrap-3-typeahead}~\cite{BT}
                for the single selection input, but had to make a few
                adjustments for the multi-selection version.

            \paragraph{Cookies and global parameters}
                In order to facilitate the transitions from one panel to the
                other, especially to keep the number of neighbors in the
                theme graph when coming back from the books bubbling, we used
                global variables.

                We also allow users to get back to their previous navigation
                on the site by storing these global parameters in the
                \texttt{localStorage} of the browser. This also allow us to
                display the tutorial (see the next paragraph) only on
                the first visit on the website.

           \begin{figure}
                  \centering
                  \includegraphics[width=.9\textwidth]{first_tutorial.png}
                  \caption{First tutorial page of the visualization}
                  \label{fig:first-tutorial}
            \end{figure}
            \paragraph{Tutorial}
                Tutorials are displayed only once at first page load, provided that the
                user accepts to let the page use the local storage of the browser. It consists in a
                chain of texts displayed on top of the page that explain different
                functionalities. An optional visual highlight can follow the
                text. This is achieved by a transparent black mask on top of the
                page, with text and highlight modified by JS code to provide
                instructions. Figure~\ref{fig:first-tutorial} gives an example of
                tutorial page.

\section{Evaluation}\label{sec:evaluation}

    In this section, we evaluate our visualization. First we will discuss the
    insights it helped us gain on the data, then we will propose a few
    possible improvements.

    \subsection{Insights about the data}\label{subsec:insights}
        \subsubsection{Clusters}\label{subsubsec:clusters}
            Even though the themes are overall extremely connected, we have gradually seen some clusters appearing. One of our most remarkable examples is the opposition fiction/non-fiction, which is present for several central themes, with different levels of clearness. This opposition is illustrated on Figure~\ref{fig:cluster}.
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{cluster.png}
                \caption{Cluster showing the opposition fiction/non-fiction.}
                \label{fig:cluster}
            \end{figure}

        \subsubsection{Weird stuff}\label{subsubsec:lol}
        \paragraph{Bücher Titelzeite} Sometimes when we fetch the cover of a
        book the language of the cover doe not match the language of the book.
        In the end it is only a minor inconvenience since the artwork is still
        relevant.

        \paragraph{Suspicious themes links} During the development we found
        ourselves playing with the visualization a lot. While wandering from
        theme to theme, we discovered some very funny associations. Some of the
        bests are: the very sexual theme \emph{``BDSM''} is only three hops away from
        ``Children'', and ``France'' is stucked between ``Fiction'' and ``Nonfiction'' with almost
        the same proximity to both.

        \paragraph{Weird themes} Some of the themes extracted by the crawlers
        made little to no sense. Often, these strange themes are only
        linked to one book or one specific author. What is also strange is that a
        lot of those weird themes were in French. Here is a short anthology :
        ``Maladie du pétrole'', ``Mikado'', ``One-shot'', and ``Bal de promo''.

        \subsubsection{Bias}\label{subsubsec:bias}
            \paragraph{Evolution} As the dataset is growing, the biases inherent to the data evolve. Also, the aforementioned characteristics of the data evolved while we were constructing our visualization. For example, with only 50,000 books, we did not see any differentiated clustering: all themes were too heavily interconnected to distinguish anything. With now three times this number, we have better insights, as presented in Section~\ref{subsubsec:clusters}.

            \paragraph{Scrapping method}As the scrapping is recursive, based on the recommendations of similar books presented on the scrapped websites, it is expected to have a bias, due to the inherent similarity of the retrieved works. As mentioned above, this bias tends to decrease when the number of books increases.

            \paragraph{Reduction of the connectivity of the graph}
            As we artificially limit the number of outgoing edges in our theme of graphs, we cannot have a full overview of the neighbors of a theme. also, limiting the depth or analysis reduces cluttering but also reduces the analysis we can conduct on our visualization.

            \paragraph{Importance of the themes}
            There is a huge difference in the number of books among the themes, eg ``Fantasy'' has 9,253 books, whereas ``Star'' has only 4. Thus, ``Fantasy tends to appear far more often on our theme graphs. This is also linked to the scrapping method as explained above.

            \paragraph{Length of the theme name}
            In our visualization, we reduce the font-size so that the name of a theme fits into its node. Thus, themes with very long names tend to be nearly unreadable, which can induce an error regarding their relative importance while reading the graph, as presented in Figure~\ref{fig:node-size}.
            \begin{figure}
                \centering
                \includegraphics[width=.9\textwidth]{node-size.png}
                \caption{Bias in the reading of the graph, due to the font size.}
                \label{fig:node-size}
            \end{figure}

            ``Historical fiction'' (top right) seems smaller than ``Comics'', though the first has 2,796 books whereas the second has only 1,890 books.
            This issue is less present in the books bubbling view, where the title is less visible.
\paragraph{User input}
We realized that the theme names are most likely manually entered by the users, without any validation whatsoever. Indeed, as presented in Figure~\ref{fig:search} the database often contains different spelling for presumably the same theme. In addition, some theme names seem totaly far-fetched to us, eg ``Maladie De La Lune'' (literally ``Sickness of the moon'').

        \subsection{Possible improvments}\label{subsec:improvments}
            \subsubsection{Allow users to tweak more parameters}\label{subsubsec:allow-user}
            There are some parameters of the visualization, such as the depth up
            to which the themes are fetched, that are not modifiable by the users
            via the UI. This could be improved but would require further work on
            the queries and force directed graph to be sure larger depth do not
            cause too much issues.

            \subsubsection{More responsive UI}\label{subsubsec:more-responsive-ui}
            Our page does not scale very well on mobile browsers. We are not CSS
            and dynamic UI experts and developed the visualization exclusively on our
            laptops. This explains why we did not put more effort into this issue.

\begin{thebibliography}{00}
    \bibitem{GR}GoodReads, \url{https://www.goodreads.com}
    \bibitem{BK}BookNode, \url{https://www.booknode.com}
    \bibitem{B}Babelio, \url{https://www.babelio.com}
    \bibitem{F}Fnac, \url{https://www.fnac.com}
    \bibitem{Am}Amazon, \url{https://www.amazon.com}
    \bibitem{WC}\texttt{d3-cloud}, by Jason Davies, \url{https://github.com/jasondavies/d3-cloud}.
    \bibitem{DAL}\texttt{d3-area-label} by Curran Kelleher, \url{https://github.com/curran/d3-area-label}.
    \bibitem{BS}\texttt{bootstrap-select} by SnapAppointments, LLC, \url{https://github.com/snapappointments/bootstrap-select}
    \bibitem{BT}\texttt{Boostrap-3-Typeahead} by bassjobsen, \url{https://github.com/bassjobsen/Bootstrap-3-Typeahead}
\end{thebibliography}

\end{document}
