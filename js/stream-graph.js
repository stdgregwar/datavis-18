class StreamGraphPlot {
    constructor(begin_date, end_date, granularity) {
        this.stop = false;
        this.pending_queries = [];
        this.granularity = granularity;
        this.data = {};
        this.keys = [];
        this.color = d3.interpolateViridis;

        this.margin = {top: 30, right: 0, bottom: -10, left: 20};
        this.width = document.body.offsetWidth - this.margin.left - this.margin.right;
        this.height = (document.body.offsetHeight - document.getElementById('mainNav').offsetHeight - 24)/2 - 14 - this.margin.top - this.margin.bottom;

        this.svg = d3.select('#stream-graph-container').append("svg")
            .attr('id', 'stream-graph')
            .attr('width', '100%')
            .attr('height', this.height + this.margin.top + this.margin.bottom);

        $('#stream-graph-container').append(`
            <div id="theme-select-container" class="theme-selection col-6 d-inline-block">
                <select class="selectpicker dropup" id="theme-select" multiple></select>
            </div>
            <div class="option-selection col-6 d-inline-block">
                <button class="btn btn-success" id="button_change_retrieval">${params.streamgraph_retrieval_strategy != "absolute" ? "Retrieve all books" : "Retrieve common books"}</button>
            </div>
        `);
        $('#theme-select').selectpicker({
            actionsBox: true,
            liveSearch: true,
            dropupAuto: false,
            size: 5,
        });
        $(".bs-searchbox input").on('keyup', () => {
            $.get(`https://api.data.lectur.begel.fr/themes_proposes/liste_autocomplete/?recherche=${$('.bs-searchbox input').val()}${params.streamgraph_retrieval_strategy == "absolute" ? '' : '&common_theme=' + params.theme_id}`, data => {
                let s = ""
                for(let i of this.keys) {
                    s += `<option value="${i}" selected>${nodes_hash[i].name}</option>`;
                }
                for(let i of data) {
                    if(this.keys.indexOf(i.id) < 0) {
                        s += `<option value="${i.id}">${i.nom_pour_langue}</option>`;
                    }
                }
                $("#theme-select").html(s)
                $("#theme-select").selectpicker('refresh')
            })
        });
        $("#theme-select").on("changed.bs.select", (e, clickedIndex, isSelected, previousValue) => {
            if (clickedIndex != undefined) {
                if(isSelected) {
                    this.query_data(e.target[clickedIndex].value, this.x.domain()[0], this.x.domain()[1], 0, true)
                } else {
                    this.keys = this.keys.filter(item => item != e.target[clickedIndex].value)
                    this.update_boundaries()
                }
                this.plot()
            }
        });
        $("button.actions-btn.bs-deselect-all.btn.btn-light").on("click", () => {
            setTimeout(() => {
                this.keys = $("#theme-select").selectpicker('val')
                this.update_boundaries()
                this.plot()
            })
        });
        $("#button_change_retrieval").on('click', () => {
            // change retrieval option
            this.stop_queries();
            params.streamgraph_retrieval_strategy = (params.streamgraph_retrieval_strategy == "absolute" ? "common" : "absolute");
            saveParams()

            // redraw graph
            Object.values(nodes_hash).forEach(node => {
                delete node.begin_date;
                delete node.end_date;
                delete node.data_for_streamgraph;
                nodes_hash[node.id] = node;
            })
            this.keys = [];
            this.query_data(params.theme_id, this.current_start_date, this.current_end_date, params.depth);
            this.stop = false;
            $("#button_change_retrieval").text(params.streamgraph_retrieval_strategy != "absolute" ? "Retrieve all books" : "Retrieve common books");
        });

        this.parseDate = d3.timeParse('%Y-%m-%d');

        // drag x-axis logic
        this.upx = Math.NaN;
        this.downx = Math.NaN;

        this.current_start_date = begin_date;
        this.current_end_date = end_date;
        this.x = d3.scaleTime()
            .range([0, this.width]);

        this.y = d3.scaleLinear()
            .range([this.height, 0]);

        this.xAxis = d3.axisBottom()
            .scale(this.x);

        this.area = d3.area()
            .x(d => this.x(d.data.date))
            .y0(d => this.y(d[0]))
            .y1(d => this.y(d[1]));

        this.stack = d3.stack()
            .offset(d3.stackOffsetWiggle)
            .order(d3.stackOrderInsideOut)
            .value((d, key) => d[key] ? d[key]["nb_books"] : 0);

        this.graph = d3.select("#stream-graph")
            .append('g')
            .attr('id', 'graph');

        d3.select("#stream-graph")
            .on("mousemove.drag", () => this.mousemove())
            .on("touchmove.drag", () => this.mousemove())
            .on("mouseup.drag",   () => this.mouseup())
            .on("touchend.drag",  () => this.mouseup());

        this.svg.append('g')
            .attr('class', 'x axis')
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(this.xAxis)
            .on("mouseenter", function() {
                d3.select('.x.axis').style("cursor", "ew-resize")
            })
            .on("mouseleave", function() {
                d3.select('.x.axis').style("cursor", "auto")
            })
            .on("mousedown.drag", () => this.xaxis_drag())
            .on("touchstart.drag", () => this.xaxis_drag());;
    }

    resize() {
        this.width = document.body.offsetWidth - this.margin.left - this.margin.right;
        this.height = (document.body.offsetHeight - document.getElementById('mainNav').offsetHeight - 24)/2 - 14;
        this.svg.attr('width', '100%')
            .attr("height", this.height);
        this.height = this.height - this.margin.top - this.margin.bottom;
        this.x = d3.scaleTime()
            .range([0, this.width]);

        this.y = d3.scaleLinear()
            .range([this.height, 0]);
        this.xAxis = d3.axisBottom()
            .scale(this.x);
        this.svg.select("g.x.axis").attr('transform', 'translate(0,' + this.height + ')');
        this.plot();
    }

    mousemove() {
        if(!isNaN(this.upx) || !isNaN(this.downx)) {
            let p = d3.mouse(document.getElementById("stream-graph"));
            d3.select('#stream-graph').style("cursor", "ew-resize");
            let rupx = this.x.invert(p[0]),
                xaxis1 = this.x.domain()[0],
                xaxis2 = this.x.domain()[1],
                xextent = xaxis2 - xaxis1;
            let changex = 1;
            if (!isNaN(this.upx) && rupx != 0) {
                changex = this.upx / rupx;
                this.current_end_date = new Date(Math.min(+xaxis1 + (xextent * changex), params.global_max_date));
                params.end_date = this.current_end_date;
                saveParams();
            };
            if (!isNaN(this.downx) && rupx != 0) {
                changex =  (+xaxis2 - this.downx) / (+xaxis2 - rupx);
                this.current_start_date = new Date(Math.max(+xaxis2 - (xextent * changex), params.global_min_date));
                params.start_date = this.current_start_date;
                saveParams();
            }
            this.plot();
            d3.event.preventDefault();
            d3.event.stopPropagation();
        }
    }

    mouseup() {
        document.onselectstart = () => true;
        d3.select('#stream-graph').style("cursor", "auto");
        if (!isNaN(this.upx) || !isNaN(this.downx)) {
            // query missing data
            this.keys.forEach(theme_id => {
                if(nodes_hash[theme_id].begin_date - this.current_start_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                    this.query_data(theme_id, this.current_start_date, nodes_hash[theme_id].begin_date, 0);
                }
            })
            this.keys.forEach(theme_id => {
                if(this.current_end_date - nodes_hash[theme_id].end_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                    this.query_data(theme_id, nodes_hash[theme_id].end_date, this.current_end_date, 0);
                }
            })
            this.plot();
            this.upx = Math.NaN;
            this.downx = Math.NaN;
            d3.event.preventDefault();
            d3.event.stopPropagation();
        };
    }

    xaxis_drag() {
        if(isNaN(this.downx) && isNaN(this.upx)) {
            document.onselectstart = () => false;
            let p = d3.mouse(document.getElementById("stream-graph"));
            let t = this.x.invert(p[0]);
            if(t <= (+this.current_start_date + +this.current_end_date) / 2) {
                this.downx = +t;
            } else {
                this.upx = +t;
            }
        }
    }

    /**
     * Format the data: expected format is list of {date, id->number of books}
     */
    formatData() {
        let d = [];
        for(let r in this.data) {
            let date = this.parseDate(r);
            if(date < this.current_end_date && date > this.current_start_date) {
                let data = this.data[r].reduce((agg, item) => {
                    agg[item["id"]] = {"nb_books": item["nb_books"], "note": 10};
                    return agg;
                }, {date: date});
                d.push(data);
            }
        }
        d.sort((a, b) => a.date - b.date);
        return d;
    }

    plot() {
        let data = this.formatData();

        if(data.length > 0) {
            const stacked = this.stack(data);
            if(stacked.length > 0) {
                this.current_y_min = d3.min(stacked, stack => d3.min(stack, d => d[0]));
                this.current_y_max = d3.max(stacked, stack => d3.max(stack, d => d[1]));
            }

            // Set domains for axes
            this.x.domain([this.current_start_date, this.current_end_date]);
            this.y.domain([
                this.current_y_min,
                this.current_y_max
            ]);

            const paths = this.graph.selectAll('path')
                .remove()
                .exit()
                .data(stacked)
                .enter().append('path')
                .attr('fill', d => this.color(1 -nodes_hash[d.key].depth / (params.depth + 1)))
                .attr('stroke', d => "#CBCBCB")
                .attr('stroke-width', d => "1")
                .attr('d', this.area);
            add_tooltip(paths, (d, i) => {
                this.graph.selectAll("path").transition()
                    .duration(100)
                    .attr("opacity", (d, j) => j != i ? 0.6 : 1);
                return this.get_tooltip_text(d);
            },
            true,
            () => this.graph.selectAll("path").transition()
                .duration(100)
                .attr("opacity", '1'),
            d => {
                tooltip.html(this.get_tooltip_text(d))
                    .style("left", (d3.event.pageX + 10) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");
            });

            if(this.stop) {
                return;
            }

            const labels = this.graph.selectAll('.area-label')
                .remove()
                .exit()
                .data(stacked)
                .enter().append('text')
                .attr('class', 'area-label')
                .text(d => nodes_hash[d.key].name)
                .attr('fill', d => "#fff")
                .attr('transform', d3.areaLabel(this.area));

            this.svg.select('.x.axis').call(this.xAxis);

            this.graph.call(
                d3.drag().on("start", () => {
                    d3.select("#stream-graph").style('cursor', 'move');
                    this.posx = this.x.invert(d3.event.x);
                    this.posy = this.y.invert(d3.event.y);
                }).on("drag", () => {
                    // move x inside range
                    let new_posx = this.x.invert(d3.event.x);
                    let delta_x = +this.posx - new_posx;
                    if((delta_x + +this.current_start_date > params.global_min_date) && (delta_x + +this.current_end_date < params.global_max_date)) {
                        this.current_start_date = new Date(delta_x + +this.current_start_date);
                        this.current_end_date = new Date(delta_x + +this.current_end_date);
                        params.start_date = this.current_start_date;
                        params.end_date = this.current_end_date;
                        saveParams();
                        // query missing data
                        this.keys.forEach(theme_id => {
                            if(nodes_hash[theme_id].begin_date - this.current_start_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                                this.query_data(theme_id, this.current_start_date, nodes_hash[theme_id].begin_date, 0);
                            }
                        });
                        this.keys.forEach(theme_id => {
                            if(this.current_end_date - nodes_hash[theme_id].end_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                                this.query_data(theme_id, nodes_hash[theme_id].end_date, this.current_end_date, 0);
                            }
                        });
                    }
                    // move y inside range
                    let new_posy = this.y.invert(d3.event.y);
                    let delta_y = this.posy - new_posy;
                    if((delta_y + this.current_y_min > this.y_min) && (delta_y + this.current_y_max < this.y_max)) {
                        this.current_y_min = delta_y + this.current_y_min;
                        this.current_y_max = delta_y + this.current_y_max;
                    }
                    this.plot();
                }).on("end", () => {
                    d3.select("#stream-graph").style('cursor', 'auto');
                    let new_posx = this.x.invert(d3.event.x);
                    let delta_x = +this.posx - new_posx;
                    if((delta_x + +this.current_start_date > params.global_min_date) && (delta_x + +this.current_end_date < params.global_max_date)) {
                        // query missing data
                        this.keys.forEach(theme_id => {
                            if(nodes_hash[theme_id].begin_date - this.current_start_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                                this.query_data(theme_id, this.current_start_date, nodes_hash[theme_id].begin_date, 0);
                            }
                        });
                        this.keys.forEach(theme_id => {
                            if(this.current_end_date - nodes_hash[theme_id].end_date > (this.granularity == "month" ? 30 : 365) * 24 * 3600 * 1000) {
                                this.query_data(theme_id, nodes_hash[theme_id].end_date, this.current_end_date, 0);
                            }
                        });
                    }
                    this.posx = NaN;
                    this.posy = NaN;
                })
            );
        }
    }

    get_tooltip_text(d) {
        // get date
        let mouse = d3.mouse(document.getElementById("stream-graph"));
        let mouseX = mouse[0];
        let date = this.x.invert(mouseX);
        // get number of books
        for(let item of nodes_hash[d.key].data_for_streamgraph) {
            if(item.month == date.getMonth() && item.year == date.getFullYear()) {
                return `<span>${nodes_hash[d.key].name}<span><br/>
                    <span>${date.toLocaleDateString("en-US", {month: 'long', year: 'numeric'})}</span><br/>
                    <span>${item.nb_books} books</span>`;
            }
        }
    }

    add_data(theme_id, min_date, max_date, data_from_server, isFromEvent) {
        let node = nodes_hash[theme_id];
        node.data_for_streamgraph = data_from_server;
        node.begin_date = node.begin_date ? new Date(Math.min(node.begin_date, min_date)) : min_date;
        node.end_date = node.end_date ? new Date(Math.max(node.end_date, max_date)) : max_date;
        nodes_hash[theme_id] = node;

        data_from_server.forEach(d => {
            d.id = theme_id;
            let date_string = `${d.year}-${d.month ? (d.month < 10 ? '0' + d.month : d.month) : '02'}-15`;
            if (this.data[date_string]) {
                this.data[date_string].push(d);
            } else {
                this.data[date_string] = [d];
            }
        });
        if (this.keys.indexOf(theme_id) < 0 && (this.keys.length < 10 || isFromEvent) && (theme_id != params.theme_id || params.streamgraph_retrieval_strategy == "absolute")) {
            this.keys.push(theme_id);
        }
        if(!isFromEvent) {
            let s = "";
            for(let i of this.keys) {
                s += `<option value="${i}">${nodes_hash[i].name}</option>`;
            }
            $("#theme-select").html(s);
            $("#theme-select").selectpicker('refresh');
            $("#theme-select").selectpicker('val', this.keys);
        }
        this.update_boundaries();
    }

    update_boundaries() {
        let full_d = [];
        for(let r in this.data) {
            let date = this.parseDate(r);
            let data = this.data[r].reduce((agg, item) => {
                agg[item["id"]] = {"nb_books": item["nb_books"], "note": 10};
                return agg;
            }, {date: date});
            full_d.push(data);
        }
        full_d.sort((a, b) => {
            return a.date > b.date;
        });
        this.stack.keys(this.keys);
        let stacked = this.stack(full_d);
        if (stacked.length > 0) {
            this.y_min = d3.min(stacked, stack => d3.min(stack, d => d[0]));
            this.y_max = d3.max(stacked, stack => d3.max(stack, d => d[1]));
            this.current_y_min = this.y_min;
            this.current_y_max = this.y_max;
        }
    }

    /** Query the needed data for the stream graph
     *
     * @int theme_id: Unique identifier of the theme to load
     * @date begin_date: Lowest date for which to retrieve data
     * @date end_date: Highest date for which to retrieve data
     * @int depth: remaining depth in the tree of neighbors
     * @bool isFromEvent: true if the call was triggered by the multi-select element
     */
    query_data(theme_id, begin_date, end_date, depth, isFromEvent=false) {
        if(depth >= 0 && (this.keys.indexOf(theme_id) < 0 || nodes_hash[theme_id].begin_date > begin_date || nodes_hash[theme_id].end_date < end_date)) {
            // if theme doesn't exist in nodes_hash
            if(!nodes_hash[theme_id]) {
                // load for nodes_hash
                let url = `https://api.data.lectur.begel.fr/themes_proposes/themes_conjoints/?keep=${params.nb_neighbors}&id=${theme_id}`;
                if(this.pending_queries.indexOf(url) < 0) {
                    this.pending_queries.push(url);
                    d3.json(url).then(data => {
                        this.pending_queries.splice(this.pending_queries.indexOf(url), 1);
                        let node = {
                            id: data.id,
                            name: data.nom,
                            nb_books: data.nb_books,
                            neighbors: data.conjoints,
                            depth: depth,
                        };
                        // stores the new node
                        nodes_hash[data.id] = node;

                        // load for stream-graph
                        d3.json(`https://api.data.lectur.begel.fr/themes_proposes/${node.id}/timeline_publications/?granularity=${this.granularity}&common_theme=${params.streamgraph_retrieval_strategy == "absolute" ? node.id : params.theme_id}&begin_year=${begin_date.getFullYear()}&end_year=${end_date.getFullYear()}&begin_month=${begin_date.getMonth()}&end_month=${end_date.getMonth()}`).then(data_for_streamgraph => {
                            this.query_for_neighbors(node, begin_date, end_date, depth, data_for_streamgraph, isFromEvent);
                        });
                    });
                }
            } else {
                let node = nodes_hash[theme_id];
                // if no data has been retrieved for the stream-graph
                if (!node.begin_date) {
                    // load for stream-graph
                    let url = `https://api.data.lectur.begel.fr/themes_proposes/${node.id}/timeline_publications/?granularity=${this.granularity}&common_theme=${params.streamgraph_retrieval_strategy == "absolute" ? node.id : params.theme_id}&begin_year=${begin_date.getFullYear()}&end_year=${end_date.getFullYear()}&begin_month=${begin_date.getMonth()}&end_month=${end_date.getMonth()}`;
                    if(this.pending_queries.indexOf(url) < 0) {
                        this.pending_queries.push(url);
                        d3.json(url).then(data_for_streamgraph => {
                            this.pending_queries.splice(this.pending_queries.indexOf(url), 1);
                            this.query_for_neighbors(node, begin_date, end_date, depth, data_for_streamgraph, isFromEvent);
                        });
                    }
                } else {
                    // if theme exists but dates don't match
                    if(node.begin_date > begin_date || node.end_date < end_date) {
                        let local_begin_date = node.begin_date > begin_date ? begin_date : node.end_date;
                        let local_end_date = node.end_date < end_date ? end_date : node.begin_date;
                        // load for wanted dates
                        let url = `https://api.data.lectur.begel.fr/themes_proposes/${node.id}/timeline_publications/?granularity=${this.granularity}&common_theme=${params.streamgraph_retrieval_strategy == "absolute" ? node.id : params.theme_id}&begin_year=${local_begin_date.getFullYear()}&end_year=${local_end_date.getFullYear()}&begin_month=${local_begin_date.getMonth()}&end_month=${local_end_date.getMonth()}`;
                        if(this.pending_queries.indexOf(url) < 0) {
                            this.pending_queries.push(url);
                            d3.json(url).then(data_for_streamgraph => {
                                this.pending_queries.splice(this.pending_queries.indexOf(url), 1);
                                data_for_streamgraph.push(...node.data_for_streamgraph);
                                this.query_for_neighbors(node, begin_date, end_date, depth, [...new Set(data_for_streamgraph)], isFromEvent);
                            });
                        }
                    } else {// the whole data is already there
                        this.query_for_neighbors(node, begin_date, end_date, depth, node.data_for_streamgraph, isFromEvent);
                    }
                }
            }
        }
    }

    query_for_neighbors(node, begin_date, end_date, depth, data, isFromEvent) {
        // launch on neighbors
        if(depth > 0 && !this.stop) {
            for(let neighbor of node.neighbors) {
                this.query_data(neighbor.id, begin_date, end_date, depth - 1, isFromEvent);
            }
        }
        // add data to stream graph
        this.add_data(node.id, begin_date, end_date, data, isFromEvent);
        if(!this.stop) {
            this.plot();
        }
    }

    stop_queries() {
        this.stop = true;
    }
}

function draw_stream_graph(theme_id, begin_date, end_date, granularity="month") {
    stream_graph_plot = new StreamGraphPlot(begin_date, end_date, granularity);

    stream_graph_plot.query_data(theme_id, begin_date, end_date, params.depth);
}

