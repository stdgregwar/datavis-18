let last_book_id = 0;

function toggleSidePanel(force=undefined) {
    if(force == undefined) {
        $("#wrapper").toggleClass("toggled");
        $('.hide-book span i').toggleClass("fa-chevron-left")
        $('.hide-book span i').toggleClass("fa-chevron-right")
    } else {
        $("#wrapper").toggleClass("toggled", force);
        $('.hide-book span i').toggleClass("fa-chevron-left", force)
        $('.hide-book span i').toggleClass("fa-chevron-right", !force)
    }
    if(stream_graph_plot != null) {
        setTimeout(() => {
            new_width = $("#stream-graph")[0].width.baseVal.value
            stream_graph_plot.width = new_width - stream_graph_plot.margin.left - stream_graph_plot.margin.right;
            stream_graph_plot.x.range([0, stream_graph_plot.width])
            stream_graph_plot.plot()
        }, 500)
    }
}

function display_book(book_id) {
    if(book_id != last_book_id) {
        let book = $("#book-display")
        book.html("");
        start_loading("#book-display", book_id, "book")
        toggleSidePanel(true)
        last_book_id = book_id;
        $.get(`https://api.data.lectur.begel.fr/livres/${book_id}/`,
            data => {
                if($('.hide-book').length == 0) {
                    $('#sidebar-wrapper').append('<div class="hide-book"><span><i class="fas fa-chevron-left"></i></span></div>');
                }
                $('.hide-book').on('click', () => {
                    toggleSidePanel()
                })
                $('#page-content-wrapper').on('click', () => {
                    if($("#wrapper").hasClass("toggled")) {
                        toggleSidePanel()
                    }
                })
                // display cover
                book.append('<div style="text-align: center">' +
                `        <img class="img-fluid" style="margin:auto;" src="https://api.data.lectur.begel.fr${data.livres[0].couvertures.couverture}">` +
                '    </div>')
                // display title
                book.append(`<p>${data.livres[0].titre}</p>`);
                // display serie + volume number
                if(data.livres[0].serie.serie_meta.est_serie) {
                    book.append(`<p>${data.livres[0].serie.titre} (${data.numero_tome ? '#' + data.numero_tome : "Novella"})</p>`);
                }
                // display authors
                let authors_names = ""
                for(let index in data.auteurs) {
                    author_name = data.auteurs[index].prenom != null ? `${data.auteurs[index].prenom} ${data.auteurs[index].nom}` :
                        `${data.auteurs[index].nom}`;
                    if(index != data.auteurs.length - 1) {
                        author_name += ', ';
                    }
                    authors_names += author_name;
                }
                book.append(`<p>by ${authors_names}</p>`);
                // display summary ? editor ?
                book.append(`<p></p>
                    <p>${data.livres[0].resumes.valeur}</p>
                    `);
                end_loading("#book-display")
            }
        );
    } else {
        if($("#book-display").html().trim() != "") {
            toggleSidePanel()
        }
    }
}
