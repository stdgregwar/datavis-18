/**
 * Display a bubble for the books related to a theme
 *
 * @int theme_id: Unique identifier of the theme for which to display the books
 * @string theme_name: Name of the theme for which to display the books
 * @int nb_books: Number of books to load
 * @int nb_recursivity: Max number of recursion allowed
 */
function draw_books_bubbling_for_theme(theme_id, theme_name, nb_books, nb_recursivity) {
    // define the url where to fetch the data
    let initial_url = `https://api.data.lectur.begel.fr/livres/for_theme/?theme=${theme_id}&limit=${nb_books}`;
    let stop_queries = false;
    start_loading("#exec", theme_id, "theme");
    let svg = d3.select("svg")
        .attr("class", "bubble");

    function resize() {
        svg.attr('width', '100%')
            .attr("height", (document.body.offsetHeight - document.getElementById('mainNav').offsetHeight - 24)/2);
        if (stream_graph_plot != null) {
            stream_graph_plot.resize();
        }
    }
    $(window).resize(resize);
    resize();

    var tooltip = d3.select("div.tooltip")
        .style("opacity", 0);

    let width = window.innerWidth,
        height = +svg.attr("height");
    // clear the svg element
    svg.html("");

    draw_back_button(theme_id);
    draw_behavior_buttons();

    let zoom_be = d3.zoom()
        .scaleExtent([.5,8])
        .on("zoom", zoomed);

    let rect = svg.append("rect");

    let g = svg.append("g"),
        bubbles = g.append("g").attr("stroke", "#fff").attr("stroke-width",1.5).selectAll(".node");
    let def = svg.append("defs").selectAll('pattern');

    rect.attr("fill", "none")
        .attr("pointer-events", "all")
        .attr("width", width)
        .attr("height", height)
        .call(zoom_be);

    function zoomed() {
        g.attr("transform", d3.event.transform);
    }
    show_bubble_graph_overlays('b_btn', 'button_add', 'button_radius', 'order_by');

    function draw(dataset) {
        // define color scale
        let color = d3.scaleOrdinal(d3.schemeCategory10);
        let bubble = d3.pack(dataset)
            .size([window.innerWidth, svg.attr("height") - svg.attr("height") / 15])
            .padding(1.5);

        let nodes = d3.hierarchy(dataset)
            .sum(d => 2 * d[params.value_radius_bubble] + 0.001);

        if (stop_queries) {
            return;
        }

        function custom_translate(ix, igrec){
            return 2;
        }

        let data = bubble(nodes).descendants();
        bubbles = bubbles
            .remove()
            .exit();
        bubbles = bubbles
            .data(data)
            .enter()
            .filter(d => !d.children)
            .append("g")
            .attr("class", "node")
            .attr("transform", d => `translate(${d.x}, ${d.y})`);
        add_tooltip(bubbles, (d, i) => `${d.data.title}<br/>Grade : ${d.data.grade}<br />${d.data.nb_votes} votes`);

        if (stop_queries) {
            return;
        }
        bubbles.append("circle")
            .attr("r", d => d.r)
            .style("fill", (d, i) => `url(#${d.data.id})`);

        if (stop_queries) {
            return;
        }

        bubbles.on('click', node => {
            d3.event.stopPropagation();
            display_book(node.data.id);
        });

        if (stop_queries) {
            return;
        }
        let new_pattern = def
            .data(dataset.children)
            .enter()
            .append('pattern')
            .attr('id', d => d.id)
            .attr('height', '100%')
            .attr('width', '100%')
            .attr('patternContentUnits', 'objectBoundingBox');
        new_pattern
            .append("image")
            .attr('height', 1)
            .attr('width', 1)
            .attr('preserveAspectRatio', 'none')
            .attr('xmlns:xlink', "http://www.w3.org/1999/xlink") // Test without this line
            .attr('xlink:href', d => `https://data.lectur.begel.fr/uploads/${d.thumbnail}`);
    }

    function draw_back_button(theme_id) {
        let current_theme_id = theme_id;

        if (stop_queries) {
            return;
        }
        $('#page-content-wrapper section').prepend(`<button class="btn btn-primary" id="b_btn"><i class="fas fa-arrow-left"></i>${theme_name}</button>`);
        $('#title_text').text(`Most popular books for theme ${theme_name}`);

        let but = $('#b_btn')
            .on('click', function() {
                stop_queries = true;
                $("#stream-graph-container").remove();
                svg.html('');
                $("#wrapper").toggleClass("toggled", false);
                $("#book-display").html("");
                $('.hide-book').remove();
                $('#b_btn').remove();
                $('#behavior_buttons').remove();
                draw_theme_graph(theme_id, params.nb_neighbors, params.depth);
            });
    }

    function draw_behavior_buttons() {
        if (stop_queries) {
            return;
        }
        $('#page-content-wrapper section').prepend(`
            <div id="behavior_buttons">
                <button class="btn btn-success w-100" id="button_add">Load more books</button>
                <div class="d-flex flex-column">
                    <button class="btn btn-secondary" id="button_radius">Change metric for radius</button>
                    <span id="radius_msg">The radius is the number of votes for a book.</span>
                </div>
                <div id="order_by">
                    <label for="order_by_select">Order by: </label>
                    <select class="selectpicker" id="order_by_select" name="order_by">
                        <option value="popular" selected>Most popular</option>
                        <option value="best_rated">Best rated</option>
                        <option value="most_voted">Most votes</option>
                    </select>
                </div>
            </div>
        `);

        $('#button_add')
            .on('click', () => {
                params.recursivity += 1;
                saveParams();
                get_data(`${initial_url}&offset=${nb_books_requested}`, 0);
            });
        $('#button_radius')
            .on('click', () => {
                params.value_radius_bubble = params.value_radius_bubble == "nb_votes" ? "diff_grade_from_mean" : "nb_votes";
                saveParams();
                $('#radius_msg').text(params.value_radius_bubble == "nb_votes" ? "The radius is the number of votes for a book." : "The radius is the grade for a book.");
                draw({"children": list_books});
            });
        $("#order_by_select").selectpicker();
        $("#order_by_select").on("changed.bs.select", (e, clickedIndex, isSelected, previousValue) => {
            if(e.target[clickedIndex].value != previousValue) {
                params.order_by = e.target[clickedIndex].value;
                saveParams();
                start_loading("#exec", theme_id, "theme");
                list_books = [];
                nb_books_requested = 0;
                get_data(`${initial_url}&sort=${params.order_by}`, nb_recursivity);
            }
        });
    }

    let list_books = [];
    let nb_books_requested = 0;
    // https://api.data.lectur.begel.fr/auteurs/author_nb_books/
    function get_data(url, nb_recursions) {
        nb_books_requested += nb_books;
        if (nb_recursions >= 0 && !stop_queries) {
            d3.json(url).then(data => {
                for (let book of data.books) {
                    if (list_books.findIndex(book_saved => book_saved.id == book.id) < 0) {
                        list_books.push({
                            'id': book.id,
                            'title': book.titre,
                            'grade': book.note.toFixed(2),
                            'diff_grade_from_mean': book.diff_mean,
                            'thumbnail': book.thumbnail,
                            'nb_votes': book.nb_votes
                        });
                    }
                }
                if (stop_queries) {
                    return;
                }
                draw({
                    "children": list_books
                });
                end_loading("#exec");
                get_data(data.next, nb_recursions - 1);
            });
        }
    }
    get_data(initial_url, nb_recursivity);
    d3.select("section.text-center.d-inline-block.w-100").append('div')
        .attr('id', 'stream-graph-container')
        .attr('class', 'row');
    draw_stream_graph(theme_id, params.start_date, params.end_date, params.granularity);
}
