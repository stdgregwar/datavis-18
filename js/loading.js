function start_loading(selector, object_id, object_type) {
    load_pending = true;
    d3.json(`https://api.data.lectur.begel.fr/citation/?${object_type}=${object_id}`).then(data => {
        if(differed_hide_loading) {
            setTimeout(() => end_loading(selector), .1)
        }
        load_pending = false;
        $(`${selector} .loading`).remove()
        if(data.quote.length > 0) {
            $(selector).append(`
                <div class="loading">
                    <p>${data.quote}</p>
                    <small>from ${data.book}</small>
                </div>`);
        }
    });
}

function end_loading(selector) {
    if(load_pending) {
        differed_hide_loading = true;
    }
    let loading = $(`${selector} .loading`)
    loading.toggleClass("hide-loading", true);
    setTimeout(() => {
        loading.remove()
    }, 3000)
}
