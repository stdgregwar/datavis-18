d3.json("https://api.data.lectur.begel.fr/stats_data/").then(data => {
    document.getElementById("stats_books").innerHTML = data['livres'];
    document.getElementById("stats_authors").innerHTML = data['auteurs'];
    document.getElementById("stats_themes").innerHTML = data['themes'];
});
