var tooltip = d3.select("div.tooltip")
    .style("opacity", 0);

function add_tooltip(d3_obj, text_prd, move=false, mouseout_callback=() => {}, mousemove_callback=() => {}) {
    d3_obj.on("mouseover", (d, i) => {
        tooltip.transition()
            .duration(200)
            .style("opacity", .9);
        let text = text_prd(d, i);
        tooltip.html(text)
            .style("left", (d3.event.pageX + (move ? 10 : 0)) + "px")
            .style("top", (d3.event.pageY - 28) + "px");
    })
    .on("mousemove", mousemove_callback)
    .on("mouseout", function(d) {
        tooltip.transition()
            .duration(500)
            .style("opacity", 0);
        mouseout_callback();
    });
}

/**
 * Draw the graph of the closest neighbors of a theme
 *
 * @int theme_id: Unique identifier of a theme
 * @int nb_neighbors: Number of neighbors to retrieve
 * @int depth: max distance between two nodes in our graph
 */
function draw_theme_graph(theme_id, nb_neighbors, depth) {
    start_loading("#exec", theme_id, "theme");
    // define the url to query from
    const base_url = `https://api.data.lectur.begel.fr/themes_proposes/themes_conjoints/?keep=${nb_neighbors}`;
    // select the svg element
    let svg = d3.select("svg");

    let stop_queries = false;
    let current_theme_id = theme_id;
    // scale it to take the whole space
    function resize() {
        svg.attr('width', '100%')
            .attr("height", document.body.offsetHeight - document.getElementById('mainNav').offsetHeight);
    }
    $(window).resize(resize);
    resize();

    // add input to select a central theme and a title
    $('#page-content-wrapper section').prepend(`<input type="text" class="typeahead" id="theme-selector" placeholder="Select your theme..."/>
        `);
    // define behavior of the select input
    let $input = $("#theme-selector");
    $input.typeahead({
        displayText: x => x.nom_pour_langue,
        afterSelect: x => redraw_theme_graph(x.id),
        source: (search, process) => $.get(`https://api.data.lectur.begel.fr/themes_proposes/liste_autocomplete/?recherche=${search}`, data => process(data)),
        theme: "bootstrap4",
    });

    // launch the tutorial
    show_theme_graph_overlays("theme-selector");

    // define size and scales
    let width = window.innerWidth,
        height = +svg.attr("height"),
        colorRamp = d3.interpolateViridis,
        nodeSize = d3.scaleLog().range([20, 70]);

    /**
     * Define the color for a node
     *
     * @object d: node to colorize
     */
    function color(d) {
        return colorRamp(1 - d.depth / (depth + 1));
    }

    // initialize storage for nodes and links
    let nodes = [];
    let links = [];

    // initialize the force-directed graph
    let simulation = d3.forceSimulation(nodes)
        .force("charge", d3.forceManyBody().strength(-2000))
        .force("link", d3.forceLink(links).strength(
            link => 0.01 * Math.log(link.source.neighbors.find(x => x.id == link.target.id).nb_common_books)
        ))
        .force("radius", d3.forceCollide().radius(node => nodeSize(node.nb_books) + 0.4).strength(1))
        .force("x", d3.forceX().strength(node => node.id == current_theme_id ? 1. : node.depth * 0.03))
        .force("y", d3.forceY().strength(node => node.id == current_theme_id ? 1. : node.depth * 0.09))
        .on("tick", ticked);

    //Create zoom behavour to control svg rect
    let zoom_be = d3.zoom()
        .scaleExtent([0.5, 8])
        .on("zoom", zoomed);

    //Add svg rect and zoom behaviour control
    let rect = svg.append("rect");

    // initialize containers for links and nodes
    let g = svg.append("g").attr("transform", `translate(${width / 2}, ${height / 2})`),
        edges = g.append("g").attr("stroke", "#999").attr("stroke-width", 1.5).selectAll(".link"),
        vertexes = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");

    //Add zoom behaviour only after the main group `g`. Sonst klapt es nicht hein
    rect.attr("fill", "none")
        .attr("pointer-events", "all")
        .attr("width", width)
        .attr("height", height)
        .call(zoom_be)
        .call(zoom_be.transform, d3.zoomIdentity.translate(width / 2, height / 2));

    function zoomed() {
        g.attr("transform", d3.event.transform);
    }

    /**
     * Function updating the graph
     */
    function restart() {
        // update the scale for the size of a node
        let minNbBooks = d3.min(nodes, d => d.nb_books);
        let maxNbBooks = d3.max(nodes, d => d.nb_books);
        nodeSize.domain([minNbBooks, maxNbBooks]);

        // set title
        if (nodes_hash[params.theme_id]) {
            params.theme_name = nodes_hash[params.theme_id].name;
            saveParams();
            $('#title_text').text(`Most linked themes for theme ${nodes_hash[params.theme_id] ? nodes_hash[params.theme_id].name : ""}`);
        } else {
            $("#title_text").text("");
        }

        // Apply the general update pattern to the nodes.
        vertexes = vertexes.data(nodes, d => d.id);
        vertexes.exit().remove();
        // for each new vertex, add a group
        new_nodes = vertexes.enter().append("g").attr("class", "group");
        add_tooltip(new_nodes, (d, i) => {
            let text = `${d.name}<br/>${d.nb_books}`;
            return text + (d.id == current_theme_id ? '<br/><b>click me!</b>' : '');
        });
        //a vertex is constitued by a circle
        new_nodes.append("circle");
        // and a text
        new_nodes.append("text")
            // "kind of" center vertically
            .attr("dy", ".2em")
            // center horizontally
            .style("text-anchor", "middle")
            // textual content
            .text(d => d.name);

        // merge old and new vertexes
        vertexes = new_nodes.merge(vertexes);

        // update the size of the vertexes
        vertexes.selectAll("circle")
            .attr("fill", color)
            .attr("id", d => d.id)
            .attr("r", d => nodeSize(d.nb_books));

        // vertexes.selectAll("text")
        //     .attr("stroke", d => colorRamp((d.depth / (params.depth + 1)) > .5 ? 0 : 1));

        // force text to stay inside the circle
        vertexes.selectAll('text')
            .attr("transform", function(d) {// can't use arow function here
                let bb = this.getBBox();
                let widthTransform = (2 * .7 * nodeSize(d.nb_books)) / bb.width;
                let heightTransform = (nodeSize(d.nb_books)) / bb.height;
                let value = widthTransform < heightTransform ? widthTransform : heightTransform;
                return `scale(${value})`;
            });
        vertexes
            // add action on clic
            .on('click', d => {
                if (d.id == current_theme_id) {
                    stop_queries = true;
                    end_loading("#exec");
                    draw_books_bubbling_for_theme(d.id, d.name, params.nb_books, params.recursivity);
                    $input.remove();
                } else {
                    redraw_theme_graph(d.id);
                }
            });


        // Apply the general update pattern to the links.
        edges = edges.data(links, function(d) {
            return Math.min(d.source.id, d.target.id) + "-" + Math.max(d.target.id, d.source.id);
        });
        edges.exit().remove();
        edges = edges
            .enter()
            .append("line")
            .attr("stroke-width", d => Math.log(d.value) + 1)
            .merge(edges);

        // Update and restart the simulation.
        simulation.nodes(nodes);
        simulation.force("link").links(links);
        simulation.alpha(1).restart();
    }

    /**
     * Redraw the graph for a new theme
     *
     * @int new_theme_id: Unique identifier of the new theme
     */
    function redraw_theme_graph(new_theme_id) {
        current_theme_id = new_theme_id;
        params.theme_id = new_theme_id;
        if (nodes_hash[new_theme_id]) {
            params.theme_name = nodes_hash[new_theme_id].name;
            saveParams();
        }

        nodes.length = 0;
        links.length = 0;
        pending_edges.length = 0;
        for (const id in nodes_hash) {
            nodes_hash[id].depth = 0;
        }
        get_and_handle_data(depth + 1, new_theme_id, new_theme_id);
    }

    /**
     * Function called on tick event
     *
     * Updates the position of the edges and vertexes
     */
    function ticked() {
        vertexes.attr("transform", d => `translate(${d.x}, ${d.y})`);

        edges.attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);
    }

    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

    // define storage for known but not rendered edges
    let pending_edges = [];

    function conj_to_edge_ids(theme, conj) {
        return {
            source: theme.id,
            target: conj.id,
            value: conj.nb_common_books
        };
    }

    function edge_id_to_edge(e) {
        return {
            source: nodes_hash[e.source],
            target: nodes_hash[e.target],
            value: e.value
        };
    }

    function push_to_links(new_links) {
        for(const nl of new_links) {
            if(links.every(l => {
                return !((l.source === nl.source && l.target === nl.target) ||
                         (l.source === nl.target && l.target === nl.source));
            })) {
                links.push(nl);
            }
        }
    }

    /**
     * Add a theme to the graph
     *
     * @int depth: max distance between two nodes in our graph
     * @int central: id of the central theme
     * @object theme: Full representation of a theme
     * @array others: list of pending theme identifiers
     */
    function add_theme_to_graph(depth, central, theme, others) {
        // split new edges into two subarrays, depending on if we can draw them or not
        let [ok_edges, nok_edges] = _.partition(theme.neighbors,
            conj => nodes.findIndex(x => x.id == conj.id) >= 0);
        let [to_insert, still_pending] = _.partition(pending_edges,
            ed => (nodes.findIndex(x => x.id == ed.target) >= 0) || ed.target == theme.id);
        pending_edges = still_pending;
        let edges = to_insert
            .concat(
                ok_edges.map(x => conj_to_edge_ids(theme, x)));

        if (nodes.findIndex(x => x.id == theme.id) < 0) {
            nodes.push(theme);
            // links.push(...edges.map(edge_id_to_edge));
            push_to_links(edges.map(edge_id_to_edge));
            restart();
        }
        end_loading("#exec");
        pending_edges.push(...nok_edges.map(x => conj_to_edge_ids(theme, x)));
        if (others != undefined && others.length > 0) {
            get_and_handle_data(depth, central, ...others);
        }
        get_and_handle_data(depth - 1, central, ...nok_edges.map(conj => conj.id));
    }

    /**
     * Retrieve and handle data from a url
     *
     * @int depth: max distance between two nodes in our graph
     * @int central: id of the central theme
     * @int theme_id: Unique identifier for which we want data
     * @array others: list of pending theme identifiers
     */
    function get_and_handle_data(depth, central, request_id, ...others) {
        if (request_id === undefined || central != current_theme_id || !depth || stop_queries) return;
        if (nodes_hash[request_id] != undefined && nodes_hash[request_id].neighbors != undefined) {
            let node = nodes_hash[request_id];
            node.depth = Math.max(node.depth, depth); //Update depth
            add_theme_to_graph(depth, central, node, others);
        } else {
            d3.json(`${base_url}&id=${request_id}`).then(data => {
                if (stop_queries) return; //Trash queries after context switch
                let node = {
                    id: data.id,
                    name: data.nom,
                    nb_books: data.nb_books,
                    neighbors: data.conjoints,
                    depth: depth
                };
                // stores the new node
                nodes_hash[data.id] = node;

                add_theme_to_graph(depth, central, node, others);
            });
        }
    }

    //get_and_handle_data(depth + 1, theme_id, theme_id);
    redraw_theme_graph(theme_id);
}
