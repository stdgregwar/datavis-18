let mate = d3.select('#help_mate');
let mate_shadow = d3.select('#help_mate_shadow');
let mate_text = $('#help_mate p');
let got_it_button = d3.select('#button_got_it');

mate.style('opacity',0);
mate.style('pointer-events','none');
mate_shadow.style('opacity',0);

function show_help_overlay(help_text, a_rect, scrollTo) {
    return new Promise(function(fullfill, reject) {
        let interrupt = false;
        let aux = (duration) => {
            if (!interrupt) {
                let rect = (a_rect && rectof(a_rect)) || {
                    width: 0,
                    height: 0,
                    left: mate_shadow.style('left'),
                    right: mate_shadow.style('right')
                };

                mate_shadow.transition()
                    .duration(duration)
                    .style('top', `${rect.top}px`)
                    .style('left', `${rect.left}px`)
                    .style('width', `${rect.width}px`)
                    .style('height', `${rect.height}px`)
                    .style('opacity', 1);
                setTimeout(() => aux(0), 100);
            }
        };
        setTimeout(() => aux(200), 0);
        if(scrollTo) {
            document.getElementById(scrollTo).scrollIntoView();
        }
        mate_text.html(help_text);
        mate.transition()
            .duration(200)
            .style('opacity', 1)
            .style('pointer-events', 'all');
        got_it_button.on('click', d => {
            interrupt = true;
            mate.transition()
                .duration(200)
                .style('opacity', 0)
                .style('pointer-events', 'none');
            mate_shadow.transition()
                .duration(200)
                .style('top', `0px`)
                .style('left', `0px`)
                .style('width', `0px`)
                .style('height', `0px`)
                .style('opacity', 0);
            fullfill();
        });
    });
}

function chain_overlays(overlay, ...others) {
    return show_help_overlay(overlay.text, overlay.rect,overlay.scrollTo).then(()=>{
        if(others.length) {
            return chain_overlays(...others);
        } else {
            return new Promise((a, r) => a());
        }
    });
}

function rectof(id) {
    elem = $(`#${id}`);
    if(elem && elem[0])
        return elem[0].getBoundingClientRect();
    else
        return undefined;
}

function show_theme_graph_overlays(input) {
    if(params.seenGraphTuto) return;
    let overlays = [
        {text: `
<h1>A world of books</h1>
<h5>A visualization of an automagically generated book database</h5>
"A world of books" aims at understanding thematic repartition of books and their relationships through several views.
Those help messages will guide you through each of them.`},
        {text:`
<h1>Theme graph view</h1>

This view is your entry point. It displays an animated graph of themes.
Themes that share a significative amount of books are linked together.
The circular nodes sizes represent the amount of books in the theme.
`},
        {text: "The search field allows you to choose a starting point in your theme exploration. Just search for a theme to load it, alongside with its nearest neighbors.",
         rect: input},
        {text: "You can hover themes to obtain some details about it. Clicking on a neighbor theme will make this neighbor the central theme. Clicking on the central theme will allow you to explore books and trends of this theme.", rect: params.theme_id}
    ];
    chain_overlays(...overlays);
    params.seenGraphTuto = true;
    saveParams();
}

function show_bubble_graph_overlays(back_btn, add_btn, radius_btn, order_by) {
    if(params.seenBubbleTuto) return;
    let overlays = [
        {text:`
<h1>Books view</h1>

Display the most popular books of the selected theme.
`},
        {text: "You can use this button to load more books in the stacked bubbles", rect: add_btn},
        {text: "Click here to change the metric that direct the look-up of the books", rect: radius_btn},
        {text: "Use this to switch between different policies", rect: order_by},
        {text: "Click this button to go back to theme exploration", rect: back_btn},
        {text: `
<h1>Themes trends</h1>

This stream graph shows the evolution of the amount of books per related themes with respect to time.

You can grab the horizontal axis to change the time scale.`,
         scrollTo:"stream-graph-container"},
        {text: "Use this button to select which themes are displayed.", rect: "theme-select-container"},
        {text: "Click this button to switch from the evolution of number of books per theme for all books to only the ones having the selected theme.", rect: "button_change_retrieval"},
    ];
    chain_overlays(...overlays);
    params.seenBubbleTuto = true;
    saveParams();
}
