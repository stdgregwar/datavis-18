// storage for all nodes
let nodes_hash = {};
// use global variables as parameters
let params = localStorage.getItem("params");
if (params === null) {
    params = {
        theme_id: 1,
        theme_name: "Fantasy",
        nb_neighbors: 5,
        depth: 4,
        nb_books: 10,
        recursivity: 1,
        start_date: new Date(2000, 1, 1),
        end_date: new Date(2015, 1, 1),
        global_min_date: new Date(1900, 1, 1),
        global_max_date: new Date(2018, 12, 1),
        granularity: "month",
        value_radius_bubble: "diff_grade_from_mean",
        order_by: "popular",
        allowedStorage : false,
        seenGraphTuto : false,
        seenBubbleTuto : false,
        streamgraph_retrieval_strategy: "absolute",
    };
} else {
    params = JSON.parse(params);
    if (params.start_date != null) {
        params.start_date = new Date(params.start_date)
    }
    if (params.end_date != null) {
        params.end_date = new Date(params.end_date)
    }
    if (params.global_min_date != null) {
        params.global_min_date = new Date(params.global_min_date)
    }
    if (params.global_max_date != null) {
        params.global_max_date = new Date(params.global_max_date)
    }
}
function saveParams() {
    if(params.allowedStorage) {
        localStorage.setItem("params", JSON.stringify(params));
    }
}

function hideDisclaimer() {
    $("#disclaimer-storage-data").css('display', "none");
    saveParams();
}

if(params.allowedStorage) {
    hideDisclaimer();
}

let load_pending = false;
let differed_hide_loading = false;

let stream_graph_plot = null;
