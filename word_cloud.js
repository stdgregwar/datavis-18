whenDocumentLoaded(() => {
    let fontSize = d3.scaleLog().range([12, 40]).domain([10, 90]);
    let layout = d3.layout.cloud()
        .size([1200, 600])
        .words([
            "Please", "wait", "we", "are", "loading", "the", "data"].map(function(d) {
                return {text: d, size: 10 + 80 * Math.random()};
            }))
        .padding(5)
        .rotate(function() { return 0; })
        .font("Impact")
        .fontSize(d => fontSize(d.size))
        .text(d => d.text)
        .on("end", draw);

    let word_cloud = d3.select("#word-cloud")
        .attr("width", layout.size()[0])
        .attr("height", layout.size()[1])
        .append("g")
        .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")

    function draw(words) {
        word_cloud
            .selectAll("text")
            .transition()
            .duration(1000)
            .style("opacity", 1e-6)
            .remove();
        word_cloud.selectAll("text").data(words)
            .enter().append("text")
            .style("font-size", function(d) { return fontSize(d.size) + "px"; })
            .style("font-family", "Impact")
            .attr("text-anchor", "middle")
        //.style("fill", function(d, i) { return fill(i); })
            .style("opacity", 1e-6)
            .attr("transform", function(d) { return "translate(" + [d.x, d.y] + ")"; })
            .transition()
            .duration(1000)
            .style("opacity", 1)
            .text(function(d) { return d.text; });
    }

    function get_words() {
        //make ajax call
        d3.json("https://api.data.lectur.begel.fr/resumes/words_occurence_count/?theme=1").then(json => {
            let words_array = [];
            for (let key of json){
                words_array.push({text: key[0], size: key[1]})
            }
            console.log(words_array)
            //render cloud
            layout.stop().words(words_array).start();
        });
    };
    layout.start();
    get_words();
});

function whenDocumentLoaded(action) {
    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", action);
    } else {
        // `DOMContentLoaded` already fired
        action();
    }
}
