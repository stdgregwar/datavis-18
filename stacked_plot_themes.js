class StackedLinePlot {
    constructor() {
        this.data = {}
        this.keys = []

        this.margin = {top: 40, right: 150, bottom: 30, left: 90},
            this.width = 800 - this.margin.left - this.margin.right, 
            this.height = 500 - this.margin.top - this.margin.bottom;

        this.parseDate = d3.timeParse('%Y-%m-%d');

        this.x = d3.scaleTime()
            .range([0, this.width]);

        this.y = d3.scaleLinear()
            .range([this.height, 0]);

        this.xAxis = d3.axisBottom()
            .scale(this.x);

        this.yAxis = d3.axisLeft()
            .scale(this.y)

        /*this.area = d3.area()
            .x(function(d) { console.log(d);
                return x(d.data.date); })
            .y0(function(d) { console.log(d); return y(d[0]); })
            .y1(function(d) { console.log(d); return y(d[1]); });*/

        this.stack = d3.stack()
        this.stack.order(d3.stackOrderNone);
        this.stack.offset(d3.stackOffsetNone);

        this.svg = d3.select("#stacked-themes")
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('id', 'graph')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

        this.svg.append ("text")
            .attr("x", 0-this.margin.left)
            .attr("y", 0-this.margin.top+20)
            .text("Evolution of the number of books per theme")

            this.svg.append('g')
                .attr('class', 'x axis')
                .attr('transform', 'translate(0,' + this.height + ')')
                .call(this.xAxis);

            this.svg.append('g')
                .attr('class', 'y axis')
                .call(this.yAxis);

        this.svg.append("g")
            .attr("class", "legend")
            .attr("transform", "translate(20, 10)")
    }

    /**
     * Format the data: expected format is list of {date, name->number of books}
     */
    formatData() {
        let d = []
        for(let r in this.data) {
            let data = this.data[r].reduce((agg, item) => {
                agg[item["nom"]] = {"nb_livres": item["nb_livres"], "note": item["note"]}
                return agg
            }, {date: this.parseDate(r)})
            d.push(data)
        }
        d.sort((a, b) => {
            return a.date > b.date
        })
        return d
    }

    plot() {
        /*************************/
        let parseDate = d3.timeParse('%Y-%m-%d');

        let x = d3.scaleTime()
            .range([0, this.width]);

        let y = d3.scaleLinear()
            .range([this.height, 0]);

        let color = d3.scaleSequential(d3.interpolateRainbow);

        let xAxis = d3.axisBottom()
            .scale(x);

        let yAxis = d3.axisLeft()
            .scale(y)

        let area = d3.area()
            .x(function(d) {
                return x(d.data.date); })
            .y0(function(d) { return y(d[0] != NaN ? d[0] : 0); })
            .y1(function(d) { return y(d[1] != NaN ? d[1] : d[0]); });

        let stack = d3.stack()
        stack.order(d3.stackOrderNone);
        stack.offset(d3.stackOffsetNone);

        let svg = d3.select('#graph');

        /************************/

        let data = this.formatData()
        let keys = this.keys

        if(data.length > 0) {
            let maxDateVal = d3.max(data, function(d){
                let vals = d3.keys(d).map(function(key){ 
                    return key !== 'date' ? (d[key] ? d[key]["nb_livres"] : 0) : 0 });
                return d3.sum(vals);
            });
            let minNote = d3.min(data, d =>
                d3.min(
                    d3.keys(d).map(
                        key => key !== 'date' ? (d[key] ? d[key]["note"]: 10) : 10
                    )
                )
            ) - 0.001
            let maxNote = d3.max(data, d =>
                d3.max(
                    d3.keys(d).map(
                        key => key !== 'date' ? (d[key] ? d[key]["note"]: 0): 0
                    )
                )
            ) + 0.0001

            // Set domains for axes
            x.domain(d3.extent(data, function(d) { return d.date; }));
            y.domain([0, maxDateVal])
            color.domain([minNote, maxNote])

            stack.keys(keys);
            stack.value((d, key) => {
                return d[key] ? d[key]["nb_livres"] : 0
            })

            let browser = svg.selectAll('.browser')
                .remove()
                .exit()
                .data(stack(data))
                .enter().append('g')
                .attr('class', function(d){ 
                    return 'browser ' + d.key; })
                .attr('fill-opacity', 0.5);

            browser.append('path')
                .attr('class', 'area')
                .attr('d', area)
                .style('fill', function(d) {
                    for(let i = data.length - 1; i >= 0; i--) {
                        if(data[i][d.key]) {
                            return color(data[i][d.key]["note"]);
                        }
                    };
                    return color(minNote)
                });

            browser.append('text')
                .datum(function(d) { return d; })
                .attr('transform', function(d) {
                    return isNaN(d[d.length - 1][1]) ? "" : 'translate(' + x(d3.max(data, d => d.date)) + ',' + y(((d[d.length-1][1] + d[d.length-1][0])/2)) + ')'; 
                })
                .attr('x', -6) 
                .attr('dy', '.35em')
                .style("text-anchor", "start")
                .text(function(d) { return (isNaN(d[d.length - 1][1]) || d[d.length - 1][0] + (maxDateVal/50) > d[d.length-1][1])? "" : d.key; })
                .attr('fill-opacity', 1);
            svg.select('.y.axis').call(yAxis)
            svg.select('.x.axis').call(xAxis)

           let legendSequential = d3.legendColor()
                .shapeWidth(30)
                .cells(10)
                .orient("horizontal")
                .scale(color)

            svg.select(".legend")
                .call(legendSequential);
        }
    }
}

function whenDocumentLoaded(action) {
    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", action);
    } else {
        // `DOMContentLoaded` already fired
        action();
    }
}

whenDocumentLoaded(() => {
    plot = new StackedLinePlot()

    let startDate = new Date(1995, 1, 1)//new Date(1950, 1, 1);
    let endDate = new Date(2018, 2, 1)//new Date(2019, 1, 1);

    function get_data(url, year, month) {
        d3.json(url).then(data_from_server => {
            if(plot.data[year + '-' + (month < 10 ? '0' + month : month) + '-01'] == null) {
                plot.data[year + '-' + (month < 10 ? '0' + month : month) + '-01'] = data_from_server
            }
            else {
                plot.data[year + '-' + (month < 10 ? '0' + month : month) + '-01'].push(...data_from_server)
            }
            plot.keys = [...new Set(data_from_server.reduce((agg, item) => {
                agg.push(item['nom'])
                return agg
            }, plot.keys))]
            plot.plot()
        })
    }
    let min_nb_books = 10;
    let min_note_theme = 8;
    let min_note_book = 9;
    let limit = 10;
    for(let year = startDate.getFullYear(); year <= endDate.getFullYear(); year++) {
        //for(let month = (year == startDate.getFullYear() ? startDate.getMonth() : 1); 
        //    month <= (year == endDate.getFullYear() ? endDate.getMonth() : 12); month++) {
        let month = 1
        setTimeout(() => {
            get_data(`https://api.data.lectur.begel.fr/themes_proposes/livres_par_theme/?year=${year}&month=${month}&min_note_book=${min_note_book}&min_note_theme=${min_note_theme}&min_nb_books=${min_nb_books}&limit=${limit}`, year, month)
        }, (year - startDate.getFullYear()) * (month - 1) * 10000)
        //   }
    }
})

