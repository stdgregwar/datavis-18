let svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height"),
    color = d3.scaleOrdinal(d3.schemeCategory10);

let nodes = [];
let links = [];

let simulation = d3.forceSimulation(nodes)
    .force("charge", d3.forceManyBody().strength(-1000))
    .force("link", d3.forceLink(links).distance(200))
    .force("x", d3.forceX())
    .force("y", d3.forceY())
    .alphaTarget(1)
    .on("tick", ticked);

let g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")"),
    link = g.append("g").attr("stroke", "#000").attr("stroke-width", 1.5).selectAll(".link"),
    node = g.append("g").attr("stroke", "#fff").attr("stroke-width", 1.5).selectAll(".node");

restart();

function add_node(node_id, node_links) {
    nodes.push(node_id);
    links.push(...node_links);
    restart();
}

function restart() {
    // Apply the general update pattern to the nodes.
    node = node.data(nodes, function(d) {
        return d.id;
    });
    node.exit().remove();
    node = node.enter().append("circle").attr("fill", function(d) {
        return color(d.id);
    }).attr("r", 8).merge(node);

    // Apply the general update pattern to the links.
    link = link.data(links, function(d) {
        return d.source.id + "-" + d.target.id;
    });
    link.exit().remove();
    link = link.enter().append("line").merge(link);

    // Update and restart the simulation.
    simulation.nodes(nodes);
    simulation.force("link").links(links);
    simulation.alpha(1).restart();
}

function ticked() {
    node.attr("cx", function(d) {
            return d.x;
        })
        .attr("cy", function(d) {
            return d.y;
        });

    link.attr("x1", function(d) {
            return d.source.x;
        })
        .attr("y1", function(d) {
            return d.source.y;
        })
        .attr("x2", function(d) {
            return d.target.x;
        })
        .attr("y2", function(d) {
            return d.target.y;
        });
}

function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}

window.onresize = () => {
    let w = window.innerWidth, h = window.innerHeight;
    console.log(w,h)
    g.attr("transform", `translate(${w / 2},${h / 2})`);
}

const base_url = "https://api.data.lectur.begel.fr/themes_proposes/themes_conjoints/?keep=5";
get_data(45,`${base_url}&id=1`);

let pending_edges = [];
let nodes_hash = {};

function get_data(depth,url, ...others) {
    if(url === undefined || !depth) return;
    console.log(`still ${others.length} pending requests`);
    d3.json(url).then(data => {
        let node = {
            id: data.id
        }; //TODO more info
        nodes_hash[data.id] = node;

        function conj_to_edge_ids(conj) {
            return {
                source: data.id,
                target: conj.id,
                value: conj.nb_common_books
            };
        }

        function edge_id_to_edge(e) {
            return {
                source: nodes_hash[e.source],
                target: nodes_hash[e.target],
                value: e.value
            };
        }

        const [ok_edges, nok_edges] = _.partition(data.conjoints,
            conj => nodes_hash.hasOwnProperty(conj.id));
        let [to_insert, still_pending] = _.partition(pending_edges,
            ed => nodes_hash.hasOwnProperty(ed.target));
        pending_edges = still_pending;
        let edges = to_insert
            .concat(
                ok_edges.map(conj_to_edge_ids));

        add_node(node,
                 edges.map(edge_id_to_edge));
        pending_edges.push(...nok_edges.map(conj_to_edge_ids));
        get_data(depth-1,...others);
        get_data(depth-1,...nok_edges.map(conj => `${base_url}&id=${conj.id}`));
    });
}
